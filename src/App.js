import React, {useEffect, useState} from 'react';
import {Box, Button, Container, CssBaseline, Grid, Paper, TextField} from "@material-ui/core";
import axios from "axios";
import {nanoid} from "nanoid";

const App = () => {
  const [items, setItems] = useState();

  const [name, setName] = useState("");
  const [message, setMessage] = useState("");



    const addItem = e => {
      e.preventDefault();
      setItems([
          ...items,
          {
              id: nanoid(),
              author: name,
              message: message,
          }
      ]);
      setName("");
  };

    const url = 'http://146.185.154.90:8000/messages';

    useEffect(() => {
        const lastDate = async () => {
            const response = await fetch(url);

            if (response.ok) {
                const newMessage = await response.json();
                setItems(newMessage)
            }
        }
    })

    // const getMessage = async () => {
    //     axios({
    //         method: 'get',
    //         data: {
    //             message: message,
    //             author: name,
    //         }
    //     });
    //     // console.log(data);
    // };


    const getMessage = async () => {
        const url = 'http://146.185.154.90:8000/messages';

        const data = new URLSearchParams();
        data.set('message', setMessage());
        data.set('author', setName());
        const response = await fetch(url, {
            method: 'get',
            body: data,
        });

        const responseData = await response.json();
    }

    const sendMessage = async (one, two) => {
        const url = 'http://146.185.154.90:8000/messages';

        const data = new URLSearchParams();

        data.set('message', one);
        data.set('author', two);
        const response = await fetch(url, {
            method: 'post',
            body: data,
        });

        const responseData = await response.json();

    };

    return (
        <Container maxWidth="md">
          <CssBaseline/>
          <form onSubmit={sendMessage(name, message)}>
            <Grid container spacing={2} alignItems="center">
              <Grid item>
                <TextField
                    label="Author name"
                    variant="outlined"
                    value={name}
                    onChange={e => setName(e.target.value)}/>
              </Grid>
                <Grid item>
                    <TextField
                        label="Message"
                        variant="outlined"
                        value={message}
                        onChange={e => setMessage(e.target.value)}/>
                </Grid>
                <Grid>
                    <Button variant="contained" color="primary" type="text">
                        Add
                    </Button>
                </Grid>

            </Grid>
          </form>

            <Grid container direction="column" spacing={2}>
                {items.map(item => (
                    <Grid item key={item.id}>
                        <Paper item component={Box} p={2}>
                            name: {item.name}
                            <p>message: {item.message}</p>
                        </Paper>
                    </Grid>
                ))}
            </Grid>
        </Container>
    );
};

export default App;